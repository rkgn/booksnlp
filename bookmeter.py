# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.7
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %%
import requests, re, emoji, sqlite3, json, urllib.parse, time, random, urllib.parse, datetime, logging, os, sys, unicodedata
from bs4 import BeautifulSoup
from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from lxml import etree
from urllib.request import urlopen

# %%
logger = logging.getLogger()
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)
logger.addHandler(handler)
logger.setLevel(logging.INFO)


# %%
class bookMeterScraper:
    def __init__(self, series_name, db_path=None, shoei_path=None, novel_type="lite_novel", author=None, is_dryrun=False, db_name="bookMeter.db"):
        self.series_name = series_name
        self.is_dryrun = is_dryrun
        if not is_dryrun:
            self.db_path = f"{db_path}/{db_name}"
            self.shoei_path = shoei_path
            if not os.path.isfile(self.db_path):
                self.__migrate(self.db_path)
            if not os.path.isdir(shoei_path):
                os.mkdir(shoei_path)
        else:
            self.db_path = None
            self.shoei_path = None
        self.meta_data = None
        self.reviews = []
        self.author = author
        self.novel_type = novel_type
        
    def __migrate(self, db_path):
        db = sqlite3.connect(db_path)
        cur = db.cursor()
        cur.execute("PRAGMA foreign_keys = true")
        cur.execute("CREATE TABLE books(isbn TEXT PRIMARY KEY, title TEXT, series TEXT ,author TEXT, volume TEXT, page_count INTEGER, published_date TEXT, publisher TEXT, description TEXT, type TEXT)")
        #cur.execute("CREATE TABLE wikidata(series TEXT, description TEXT, FOREIGN KEY (series) REFERENCE books(series))")
        cur.execute("CREATE TABLE reviews(id INTEGER PRIMARY KEY AUTOINCREMENT, isbn TEXT, review TEXT, star INTEGER, is_netabare INTEGER, FOREIGN KEY (isbn) REFERENCES books(isbn))")
        cur.execute("CREATE TABLE histories(isbn TEXT PRIMARY KEY, time_stamp INTEGER, is_finished INTEGER, FOREIGN KEY (isbn) REFERENCES books(isbn))")
        db.commit()
        db.close()
        
    def __getMetaData(self, series_name, author, novel_type):
        logger.info(f"{series_name}のメタデータを取得しています")
        result = []
        g_api_url = "https://www.googleapis.com/books/v1/volumes"
        title = urllib.parse.quote(re.sub("シリーズ$", "", series_name))
        if author is None:
            f = urlopen(f"https://iss.ndl.go.jp/api/opensearch?title={title}")
        else:
            author = urllib.parse.quote(author)
            f = urlopen(f"https://iss.ndl.go.jp/api/opensearch?title={title}&creator={author}")
        xml = f.read()
        root = etree.fromstring(xml)
        for item in root.iter("item"):
            isbn = item.find("dc:identifier[@xsi:type='dcndl:ISBN']", root.nsmap)
            ndc = item.find("dc:subject[@xsi:type='dcndl:NDC9']", root.nsmap)
            if isbn is None or ndc is None:
                continue
            else:
                ndc = float(ndc.text)
                if ndc != 913.6:
                    continue
            title = item.find("title").text
            isbn = isbn.text
            if [ isbn for dic in result if dic["isbn"] == isbn ]:
                continue
            volume = item.find("dcndl:volume", root.nsmap)
            if volume is None:
                volume = "1"
            else :
                #volume = int(re.sub("[^0-9]+","",volume.text))
                volume = volume.text
            author = item.find("dc:creator", root.nsmap)
            if author is None:
                author = item.find("author").text
            else:
                author = author.text
            publisher = item.find("dc:publisher", root.nsmap).text
            pd = item.find("dc:date", root.nsmap)
            if not pd is None:
                pd = pd.text
                pd = unicodedata.normalize("NFKC", pd)
                pd = re.sub("月|(\(.+\))|-", "", re.sub("年", ".", pd))
                pd = pd[0:4] + "." + pd.split(".")[1].zfill(2) if len(pd) > 5 else pd
                published_date = pd.replace(" ", "")
            else:
                published_date = None
            res = json.loads(requests.get(f"{g_api_url}?q=isbn:{isbn}").text)
            if res["totalItems"] == 0:
                published_date = page_count = description = None
            else:
                res_info = res["items"][0]["volumeInfo"]
                if not "pageCount" in res_info:
                    page_count = None
                else:
                    page_count = res_info["pageCount"]
                if not "description" in res_info:
                    description = None
                else:
                    description = res_info["description"]
            result.append({"isbn" : isbn, "title" : title, "series" : series_name, "author" : author, "volume" : volume,
                           "page_count" : page_count, "published_date" : published_date, "publisher" : publisher, "description" : description, "novel_type" : novel_type})
            
        logger.info(f"{len(result)}件の書籍がヒットしました")
        return result
            
    def __scrapeFromBookMeter(self, isbn, driver, shoei_path):
        logger.info(f"ISBN:{isbn}のスクレイピングをスタートします")
        url = "https://bookmeter.com"
        driver.get(f"{url}/search?keyword={isbn}")
        html = driver.page_source
        soup = BeautifulSoup(html, "html.parser")
        title_elem = soup.find("div", attrs={ "class" : "detail__title"})
        if title_elem is None:
            logger.info(f"ISBN:{isbn}を読書メーターで発見できませんでした。スキップします")
            return []
        id = int(re.sub("^\/books/", "", title_elem.contents[0].get("href")))
        time.sleep(random.uniform(1, 3))
        driver.get(f"{url}/books/{id}")
        html = driver.page_source
        soup = BeautifulSoup(html, "html.parser")
        if not self.is_dryrun:
            img_url = soup.find("a", attrs={ "class" : "image__cover"}).find("img").get("src")
            driver.get(img_url)
            img = driver.find_element(By.XPATH, "/html/body/img")
            shoei_path = re.sub("\/$", "", shoei_path)
            with open(f'{shoei_path}/{isbn}.png', 'wb') as f:
                f.write(img.screenshot_as_png)
        driver.get(f"{url}/books/{id}")
        html = driver.page_source
        soup = BeautifulSoup(html, "html.parser")
        review_cnt = int(soup.find("div", attrs={ "class" : "content__count"}).get_text())
        logger.info(f"{review_cnt}件のレビューが見つかりました")
        last_page = review_cnt//40+1
        reviews = []
        for i in range(1, last_page+1):
            logger.info(f"{i}ページ目を処理中...")
            time.sleep(random.uniform(1, 3))
            driver.get(f"{url}/books/{id}?page={i}")
            html = driver.page_source
            soup = BeautifulSoup(html, "html.parser")
            reviews_html = soup.find_all('div', attrs={ 'class': 'frame__main' })
            for review_html in reviews_html:
                is_netabare = review_html.find('span', attrs={'class': 'frame__content__text__netabare-icon'}) != None
                text = review_html.find("div", attrs={ "class" : "frame__content__text"}).get_text()
                if(len(text) > 50 and not (re.search("http:\/\/(.+\/)+|https:\/\/(.+\/)+", text) ) ):
                    star_elem = review_html.find("a", attrs={ "class" : "frame__details__nice-counter"})
                    if star_elem is None :
                        star = 0
                    else:
                        star = int(star_elem.get_text().replace("★",""))
                    text = re.sub("[★☆]+|ネタバレ", "", text)
                    text = re.sub("[\u3000 \t]", " ", text)
                    text = emoji.replace_emoji(text)
                    reviews.append({"text" : text, "star" : star, "is_netabare": is_netabare}) 
        logger.info(f"ISBN:{isbn}のスクレイピングが完了しました")
        return reviews
    
    def scrape(self):
        try:
            self.meta_data = self.__getMetaData(series_name = self.series_name, author = self.author, novel_type=self.novel_type)
        except Exception as e:
            logger.error("エラーが発生しました")
            logger.error(e)
            return
        isbns = [dic["isbn"] for dic in self.meta_data]
        if not self.is_dryrun:
            db = sqlite3.connect(self.db_path)
            cur = db.cursor()
            if not cur.execute("SELECT * FROM books WHERE series = ?", (self.series_name, )).fetchone() is None:
                logger.info(f"シリーズ:{self.series_name}は既に登録済みです。スクレイピング処理に移ります")
            else:
                for md in self.meta_data:
                    if cur.execute("SELECT * FROM books WHERE isbn=?", (md["isbn"], )).fetchone() is None:
                        cur.execute("INSERT INTO books VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",(md["isbn"], md["title"], md["series"], md["author"], md["volume"], md["page_count"], md["published_date"], md["publisher"], md["description"], md["novel_type"]))
                        db.commit()
        options = Options()
        options.add_argument('-headless')
        driver = webdriver.Firefox(options=options)
        for isbn in isbns:
            if not self.is_dryrun:
                if not cur.execute("SELECT * FROM histories WHERE isbn = ?", (isbn, )).fetchone() is None:
                    if cur.execute("SELECT is_finished FROM histories WHERE isbn = ?", (isbn, )).fetchone()[0] == 0:
                        cur.execute("DELETE FROM reviews WHERE isbn = ?", (isbn, ))
                    else:
                        logger.info(f"ISBN:{isbn}は処理が完了しているのでスキップします")
                        continue
                else:
                    cur.execute("INSERT INTO histories VALUES(?, ?, ?)", (isbn, int(time.time()), 0))
                    db.commit()
            try:
                reviews = self.__scrapeFromBookMeter(isbn=isbn, driver=driver, shoei_path=self.shoei_path)
            except Exception as e:
                logger.error(f"ISBN:{isbn}エラーが発生しました")
                logger.error(e)
                continue
            self.reviews.append(reviews)
            if not self.is_dryrun:
                for review in reviews:
                    cur.execute("INSERT INTO reviews(isbn, review, star, is_netabare) VALUES(?, ?, ?, ?)", (isbn, review["text"], review["star"], int(review["is_netabare"])))
                    db.commit()
                    if(len(review) != 0):
                        cur.execute("UPDATE histories SET is_finished = 1 WHERE isbn = ?", (isbn, ))
                db.commit()
        driver.quit()
        if not self.is_dryrun:
            db.close()

# %%
