# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.7
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %%
import spacy, sqlite3, MeCab, re, tomotopy, unicodedata, requests
from spacy import displacy
from bs4 import BeautifulSoup
import matplotlib.pyplot as plt
import numpy as np
from tqdm.auto import tqdm
import pandas as pd

# %%
sentence = "カノ"
nlp = spacy.load('ja_ginza_electra')
doc = nlp(sentence)
displacy.render(doc, style="ent", jupyter=True)


# %%
def wakachiGaki(dic_path, sentence, resque=[]):
    wakachi = MeCab.Tagger(f"-r /dev/null -d {dic_path}")
    node = wakachi.parseToNode(sentence)
    sentence_tmp = []
    sentence_result = []
    while node:
        node_f = node.feature.split(",")
        if node.surface in resque:
            sentence_result.append(node_f[6])
            node = node.next
            continue
        if node_f[0] in ["名詞", "形容詞"]:
            sentence_tmp.append(node_f[6])
        node = node.next
    for w in sentence_tmp:
        node = wakachi.parseToNode(w)
        while node:
            node_f = node.feature.split(",")
            if re.match("[0-9一二三四五六七八九十上下]+巻", node.surface):
                pass
            else:
                if node_f[2] != "人名" and len(node_f[6]) > 1:
                    if node_f[1] == "固有名詞":
                        sentence_result.append(node.surface)
                    else:
                        sentence_result.append(node_f[6])
            node = node.next
    return sentence_result


# %%
def removeNE(sentence, resque=[]):
    nlp = spacy.load('ja_ginza_electra')
    doc = nlp(sentence)
    for ent in doc.ents:
        if ent.label not in ["Position_Vocation", "Date", "School_Age", "Sport"] and ent.text not in resque:
            sentence = sentence.replace(ent.text, "固有名詞")
    return sentence


# %%
def cos_sim(v1, v2):
    return np.dot(v1, v2) / (np.linalg.norm(v1) * np.linalg.norm(v2))


# %%
def getReviewsFromTitle(title):
    db = sqlite3.connect("data/bookMeter.db")
    cur = db.cursor()
    res = [r[0] for r in cur.execute("SELECT review FROM (SELECT * FROM reviews INNER JOIN books ON reviews.isbn = books.isbn) WHERE series = ?", (title, )).fetchall()]
    db.close
    return res


# %%
after = wakachiGaki(sentence=res, dic_path="/usr/lib/x86_64-linux-gnu/mecab/dic/mecab-ipadic-neologd")

# %%
sw = []
db = sqlite3.connect("data/bookMeter.db")
cur = db.cursor()
series = cur.execute("SELECT DISTINCT series FROM books").fetchall()
series = [s[0] for s in series]
series = series + [s + "シリーズ" for s in series]
sw = sw + series
db.close()
url = "http://svn.sourceforge.jp/svnroot/slothlib/CSharp/Version1/SlothLib/NLP/Filter/StopWord/word/Japanese.txt"
sw = [sw for sw in requests.get(url).text.split() if sw != ""] + sw

# %%
corpus = tomotopy.utils.Corpus(stopwords=sw)

# %%
# %%time
db = sqlite3.connect("data/bookMeter.db")
dic_p = "/usr/lib/x86_64-linux-gnu/mecab/dic/mecab-ipadic-neologd"
cur = db.cursor()
all_reviews =  []
series = cur.execute("SELECT DISTINCT series FROM books").fetchall()
series = [s[0] for s in series]
for s in tqdm(series):
    isbns = cur.execute("SELECT isbn FROM books WHERE series=?", (s, )).fetchall()
    isbns = [isbn[0] for isbn in isbns]
    reviews_tmp = []
    for isbn in isbns:
        reviews = [review[0] for review in cur.execute("SELECT review FROM reviews WHERE isbn=?", (isbn, )).fetchall()]
        reviews = [re.sub('["…【】「」｛｝（）()\/、。・▲？?！!～￥^♪○◯●∇〈〉《》『』─＜＞◇［］≒＾〜]|(･･･)', "", review) for review in reviews]
        reviews = [unicodedata.normalize("NFKC", review) for review in reviews]
        reviews = [removeNE(review, resque=["百合"]) for review in reviews]
        r = wakachiGaki(dic_path=dic_p, sentence="".join(reviews))
        reviews_tmp += r
    all_reviews.append({"series" : s, "reviews" : reviews_tmp})
    corpus.add_doc(reviews_tmp)
db.close()
with open('data/reviews5.json', 'w') as f:
    json.dump(all_reviews, f, indent=2)
corpus.save(filename="data/corpus5")

# %%
reviews = []
url = "http://svn.sourceforge.jp/svnroot/slothlib/CSharp/Version1/SlothLib/NLP/Filter/StopWord/word/Japanese.txt"
sw = [sw for sw in requests.get(url).text.split() if sw != ""]
for r in tqdm(getReviewsFromTitle("ヴァイオレット・エヴァーガーデン")):
    r = re.sub('["…【】「」｛｝（）()\/、。・▲？?！!～￥^♪○◯●∇〈〉《》『』─＜＞◇［］≒＾〜]|(･･･)', "", r)
    r = unicodedata.normalize("NFKC", r)
    tmp = wakachiGaki(sentence=removeNE(sentence=r, resque="百合"), dic_path="/usr/lib/x86_64-linux-gnu/mecab/dic/mecab-ipadic-neologd")
    reviews += [t for t in tmp if not t in sw + ["*"]]

# %%
pd.DataFrame(pd.DataFrame(reviews).value_counts()).head(50)

# %%
