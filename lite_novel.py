# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.7
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %%
import requests, bookmeter, re, tomotopy, MeCab, sqlite3, json, numpy, unicodedata, os, shutil, urllib.parse, seaborn, random, copy
from bs4 import BeautifulSoup
import matplotlib.pyplot as plt
import numpy as np
from tqdm.auto import tqdm
from lxml import etree
from urllib.request import urlopen
import pandas as pd
pd.set_option('display.max_columns', 100)
import pickle
from gensim import corpora
from gensim import models
from transformers import pipeline
from wordcloud import WordCloud


# %% [markdown]
# ## 関数群

# %%
def getLiteNovelData():
    url = "https://ja.wikipedia.org/wiki/%E3%81%93%E3%81%AE%E3%83%A9%E3%82%A4%E3%83%88%E3%83%8E%E3%83%99%E3%83%AB%E3%81%8C%E3%81%99%E3%81%94%E3%81%84!"
    res = requests.get(url)
    soup = BeautifulSoup(res.text, "html.parser")
    tables = soup.find_all("table")
    tables = tables[1:4]
    results = []
    for table in tables:
        trs = table.find_all("tr")
        for tr in trs:
            if (not tr.find("th") is None) or len(tr.find_all("td")) < 3:
                continue
            if len(tr.find_all("td")) == 5:
                title_i = 0
            else:
                title_i = 1
            title = tr.find_all("td")[title_i].text
            author = tr.find_all("td")[title_i + 1].text
            title = re.sub('["<>〈〉]', "", title)
            if [result for result in results if result["title"] == title]:
                continue
            results.append({"title" : title, "author" : author})
    return results


# %%
def wakachiGaki(dic_path, sentence, resque=[]):
    wakachi = MeCab.Tagger(f"-r /dev/null -d {dic_path}")
    node = wakachi.parseToNode(sentence)
    sentence_tmp = []
    sentence_result = []
    while node:
        node_f = node.feature.split(",")
        if node.surface in resque:
            sentence_result.append(node_f[6])
            node = node.next
            continue
        if node_f[0] in ["名詞", "形容詞"]:
            sentence_tmp.append(node_f[6])
        node = node.next
    for w in sentence_tmp:
        node = wakachi.parseToNode(w)
        while node:
            node_f = node.feature.split(",")
            if re.match("[0-9一二三四五六七八九十上下]+巻", node.surface):
                pass
            else:
                if node_f[2] != "人名" and len(node_f[6]) > 1:
                    if node_f[1] == "固有名詞":
                        sentence_result.append(node.surface)
                    else:
                        sentence_result.append(node_f[6])
            node = node.next
    return sentence_result


# %%
def cos_sim(v1, v2):
    return np.dot(v1, v2) / (np.linalg.norm(v1) * np.linalg.norm(v2))


# %% [markdown]
# ## 「このライトノベルがすごい！」受賞作品をスクレイピング

# %%
for d in data:
    s = bookmeter.bookMeterScraper(series_name=d["title"], db_path="data", shoei_path="data/shoei", author=d["author"], novel_type="lite_novel")
    s.scrape()

# %%
s = bookmeter.bookMeterScraper(series_name="", db_path="data", shoei_path="data/shoei", novel_type="lite_novel")
s.scrape()

# %% [markdown]
# ## データ前処理

# %%
db = sqlite3.connect("data/bookMeter.db")
cur = db.cursor()
result = []
for s in tqdm([r[0] for r in cur.execute("select distinct series from books").fetchall()]):
    isbns = [i[0] for i in cur.execute("select isbn from books where series = ?", (s, ))]
    cnt = [cur.execute("select sum(length(review)) from reviews where isbn = ?", (isbn, )).fetchone()[0] for isbn in isbns]
    cnt = sum(filter(None, cnt))
    if cnt >= 10000:
        result.append({"series" : s, "word_cnt" : cnt })
result

# %%
plt.bar(list(range(len(result))), [r["word_cnt"] for r in result])

# %%
sw = ["シリーズ", "ロレンス", "スバル", "しまむら", "マイン", "上条さん", "物語シリーズ", "ベル", "作品", "アリシゼーション", "キノ", "物語", "作品", "ない", "いい", "好き", "再読", "イクタ", "阿良", "八幡", "撫子", "羽川",
      "カリエ", "インデックス", "ミーア", "ゴブリンスレイヤー", "ナザリック", "アインズ", "アリソン", "リムル", "クトリ", "ヴィレム","ラノベ", "ライトノベル", "アニメ", "主人公", "あとがき", "大河", "藻屑", "コル", "面白かっ",
     "一方通行", "小説", "キノの旅" ,"学園都市", "雪ノ下", "セルティ", "帝人", "平塚", "由比ヶ浜", "友崎", "アド", " ケイ", "杏里", "戯言シリーズ", "文学少女", "ちーちゃん", "理科", "ミミズク", "シエスタ", "夜空", "いろはす",
     "銀子", "読了", "シズちゃん", "バッカーノ", "九曜", "やせ", "エンテイスラ", "アイリス", "真昼", "白粉", "まーちゃん", "フルメタ", "ケイ", "ホライゾン", "デュラララ", "バカテス", "かなめ", "本編", "ウィズ", "戯言", "彼ら",
     "バカテス", "成田", "化物語", "86", "ヒッキー", "ページ", "シーズン", "シーン", "ティー", "あい", "サイド", "新約", "池袋", "さま", "モモ", "たつ", "部分", "ほしい", "SOS団", "古泉", "潤さん", "クビシメロマンチスト",
     "コウ", "サクラ", "ノット", "シカ", "ザカリア", "すごい", "BACCANO!", "アイザック", "良い", "多い", "よい", "カラ", "東和", "完結", "テン", "トエ", "からすみ", "クロハ", "フェリオ", "パンプキン", "リセ", "7 Iris",
     "ALISON", "メグセロ", "リリトレ", "リリアとトレイズ", "ドクロちゃん", "ジーク", "長編", "砂糖", "短編", "長い", "代行", "Hickey", "DELE", "無い", "短編集", "秀麗", "症候群", "お話", "ライダー", "Fate", "Web", "妖精さん",
     "やすい", "古城", "古典部", "前半", "後半", "少佐", "時雨", "弁当", "一番", "原作", "娘。", "後宮", "グレイ", "マスター", "既読", "アニメ化", "禁書", "面白い", "2人", "ミステリ", "最後", "師匠", "SS", "印象", "エピローグ",
     "作家", "最終", "嬉しい", "登場", "リセット", "今後", "期待", "強い", "弟子", "楽しみ", "意味", "日本", "期待", "外伝", "パーティ", "タイトル", "最初", "欲しい", "大きい", "続く", "っぽい", "問題", "楽しい",
     "作り", "っぷり", "厄介", "プロローグ", "非常", "サイト", "偽物", "ただ", "ラスト", "デビュー作", "にくい", "おもしろい", "周り", "軽い", "メイン", "状況", "説明", "Kindle", "エロ", "おっぱい", "出し手", "すぎ", "劇場版",
     "づらい", "個人的", "続編", "書籍", "前編", "後編", "エッチ", "WWW", "打ち切り", "連作", "スピンオフ", "電撃文庫", "ロリコン", "ロリ", "5人"]
db = sqlite3.connect("data/bookMeter.db")
cur = db.cursor()
series = [r["series"] for r in result]
sw = sw + series
db.close()
url = "http://svn.sourceforge.jp/svnroot/slothlib/CSharp/Version1/SlothLib/NLP/Filter/StopWord/word/Japanese.txt"
sw = [sw for sw in requests.get(url).text.split() if sw != ""] + sw

# %%
# %%time
db = sqlite3.connect("data/bookMeter.db")
lab_dic_p = "/usr/lib/x86_64-linux-gnu/mecab/dic/mecab-ipadic-neologd"
cur = db.cursor()
all_reviews =  []
series = [r["series"] for r in result]
for s in tqdm(series):
    isbns = cur.execute("SELECT isbn FROM books WHERE series=?", (s, )).fetchall()
    isbns = [isbn[0] for isbn in isbns]
    reviews_tmp = []
    for isbn in isbns:
        reviews = [review[0] for review in cur.execute("SELECT review FROM reviews WHERE isbn=?", (isbn, )).fetchall()]
        reviews = [re.sub('["…【】「」｛｝（）()\/、。・▲？?！!～￥^♪○◯●∇〈〉《》『』─＜＞◇［］≒＾〜]|(･･･)', "", review) for review in reviews]
        reviews = [unicodedata.normalize("NFKC", review) for review in reviews]
        r = wakachiGaki(dic_path=lab_dic_p, sentence="".join(reviews), resque=["百合"])
        reviews_tmp += r
    all_reviews.append({"series" : s, "reviews" : [r for r in reviews_tmp if not r in sw]})
db.close()

# %%
with open('data/reviews.json', 'w') as f:
    json.dump(all_reviews_rm_NE, f, indent=2)

# %%
plt.bar(list(range(len(all_reviews))), [len(r["reviews"]) for r in all_reviews])

# %%
# %%time
all_reviews_rm_NE = []
for index, review in enumerate(tqdm(all_reviews)):
    other_reviews = []
    [other_reviews.extend(list(set(r["reviews"]))) for i, r in enumerate(all_reviews) if i != index]
    ne = [word for word in set(review["reviews"]) if not word in other_reviews]
    all_reviews_rm_NE.append({"series" : review["series"], "reviews" : [word for word in review["reviews"] if not word in ne]})

# %%
with open('data/reviews_rm_NE.json', 'w') as f:
    json.dump(all_reviews_rm_NE, f, indent=2)

# %%
with open('data/reviews_rm_NE.json') as f:
    all_reviews_rm_NE = json.load(f)

# %% [markdown]
# ## 上限5万単語，TF-IDFで水増し

# %%
reviews_list = [r["reviews"] for r in all_reviews_rm_NE_max_5e4]

# %%
dictionary = corpora.Dictionary(reviews_list)

# %%
corpus = list(map(dictionary.doc2bow, reviews_list))
test_model = models.TfidfModel(corpus)
corpus_tfidf = test_model[corpus]

 # %%
 sorted(corpus_tfidf[0], key=lambda ti: ti[1], reverse=True)


# %%
def roulette(tf_idf_list, dic):
    tf_idf_list = sorted(tf_idf_list, key=lambda ti: ti[1], reverse=True)
    tf_idf_sum = sum([ti[1] for ti in tf_idf_list])
    rnd = random.uniform(0, tf_idf_sum)
    tf_idf_sum = 0
    for tf_idf in tf_idf_list:
        tf_idf_sum += tf_idf[1]
        if tf_idf_sum > rnd:
            return dic[tf_idf[0]]


# %%
len(reviews_list[100])

# %%
[roulette(corpus_tfidf[100], dictionary) for _ in range(10)]

# %%
reviews_adjusted = copy.deepcopy(all_reviews_rm_NE_max_5e4)
for i, title in enumerate(tqdm(all_reviews_rm_NE_max_5e4)):
    wc = len(title['reviews'])
    if wc < 5e4:
        reviews_adjusted[i]["reviews"] += [roulette(corpus_tfidf[i], dictionary) for _ in range(int(5e4 - wc))]

# %%
with open('data/reviews_adjusted.json', 'w') as f:
    json.dump(reviews_adjusted, f, indent=2)

# %%
with open('data/reviews_adjusted.json') as f:
    reviews_adjusted = json.load(f)

# %%
all_reviews_rm_NE_max_5e4 = copy.deepcopy(all_reviews_rm_NE)
for s_i, s in enumerate(tqdm(all_reviews_rm_NE)):
    r_l = len(s["reviews"])
    if r_l > 5e4:
        delete_i = []
        for i in range(r_l - int(5e4)):
            while True:
                rnd = random.randint(0, (r_l - 1) )
                if not rnd in delete_i:
                    delete_i.append(rnd)
                    break
        all_reviews_rm_NE_max_5e4[s_i]["reviews"] = [r for i, r in enumerate(s["reviews"]) if not i in delete_i]

# %%
with open('data/reviews_rm_NE_max_5e4.json', 'w') as f:
    json.dump(all_reviews_rm_NE_max_5e4, f, indent=2)

# %%
with open('data/reviews_rm_NE_max_5e4.json') as f:
    all_reviews_rm_NE_max_5e4 = json.load(f)

# %%
plt.bar(list(range(len(all_reviews_rm_NE_max_5e4))), [len(r["reviews"]) for r in all_reviews_rm_NE_max_5e4])

# %%
all_reviews_rm_NE_max_1e4 = copy.deepcopy(all_reviews_rm_NE)
for s_i, s in enumerate(tqdm(all_reviews_rm_NE)):
    r_l = len(s["reviews"])
    if r_l > 1e4:
        delete_i = []
        for i in range(r_l - int(1e4)):
            while True:
                rnd = random.randint(0, (r_l - 1) )
                if not rnd in delete_i:
                    delete_i.append(rnd)
                    break
        all_reviews_rm_NE_max_1e4[s_i]["reviews"] = [r for i, r in enumerate(s["reviews"]) if not i in delete_i]

# %%
plt.bar(list(range(len(all_reviews_rm_NE_max_5e4))), [len(r["reviews"]) for r in all_reviews_rm_NE_max_5e4])

# %%
pd.DataFrame(pd.DataFrame(all_reviews_rm_NE[0]["reviews"]).value_counts()).head(10)

# %%
corpus = tomotopy.utils.Corpus(stopwords=sw)

# %%
[corpus.add_doc(r["reviews"]) for r in tqdm(reviews_adjusted)];

# %%
corpus2 = tomotopy.utils.Corpus(stopwords=sw)

# %%
[corpus2.add_doc(r["reviews"]) for r in tqdm(all_reviews_rm_NE_max_1e4)];

# %%
with open('data/reviews5.json', 'w') as f:
    json.dump(all_reviews_rm_NE, f, indent=2)

# %%
with open('data/reviews5.json') as f:
    all_reviews_rm_NE = json.load(f)

# %%
corpus.save(filename="data/corpus_final")

# %%
corpus = tomotopy.utils.Corpus.load("data/corpus5")

# %%
with open('data/reviews4.json') as f:
    all_reviews = json.load(f)

# %%
# %%time
i_max = 1001
k_max = 41
c_avg = []
p_avg = []
for i in tqdm(range(10, i_max, 10)):
    c = []
    p = []
    for k in tqdm(range(5, k_max)):
        model = tomotopy.LDAModel(k=k, corpus=corpus)
        model.train(iter=i)
        c.append(tomotopy.coherence.Coherence(model).get_score())
        p.append(model.perplexity)
    c_avg.append(sum(c)/len(c))
    p_avg.append(sum(p)/len(p))

# %%
fig = plt.figure()
ax1 = fig.add_subplot(111)
c1 = 'lightcoral'
ax1.set_xlabel('k')
ax1.plot(list(range(10, 1001, 10)), c_avg, 'o-', color=c1)
ax1.set_ylabel('Coherence Avg.', color=c1); ax1.tick_params('y', colors=c1)
#ax1.set_xticks(np.arange(10, 1001, step=10))
fig.tight_layout()
plt.grid(axis="y")
fig.set_size_inches(15, 4)
plt.show()

# %%
# %%time
k_max = 41
cf_max = 101
df_max = 101
coherence = []
perplexity = []
for iter_i in tqdm([10, 100, 500]):
    c_i = []
    p_i = []
    for cf in range(0, cf_max, 10):
        c_cf = []
        p_cf = []
        for df in range(0, df_max, 10):
            c_df = []
            p_df = []
            for k in range(5, k_max):
                model = tomotopy.LDAModel(k=k, min_cf=cf, min_df=df,corpus=corpus)
                model.train(iter=iter_i)
                c_df.append(tomotopy.coherence.Coherence(model).get_score())
                p_df.append(model.perplexity)
            c_cf.append(c_df)
            p_cf.append(p_df)
        c_i.append(c_cf)
        p_i.append(p_cf)
    coherence.append(c_i)
    perplexity.append(p_i)
with open('data/coherence.txt', 'wb') as f:
    pickle.dump(coherence, f)
with open('data/perplexity.txt', 'wb') as f:
    pickle.dump(perplexity, f)

# %%
coherence

# %%
with open('data/coherence.txt', 'rb') as p:
    c = pickle.load(p)
c

# %%
result_df = pd.DataFrame({i_cf*10: { i_df*10: sum(c_df)/len(c_df) for i_df, c_df in enumerate(c_cf)} for i_cf, c_cf in enumerate(c[0]) }).head(11)

# %%
result_df

# %%
fig = plt.figure()
fig.tight_layout()
p = seaborn.heatmap(result_df, annot=True, fmt=".3f", square=True)
plt.xlabel("min_cf")
plt.ylabel("min_df")
plt.gcf().set_size_inches(12, 10)

# %%
result_df100 = pd.DataFrame({i_cf*10: { i_df*10: sum(c_df)/len(c_df) for i_df, c_df in enumerate(c_cf)} for i_cf, c_cf in enumerate(c[1]) }).head(11)

# %%
fig = plt.figure()
fig.tight_layout()
p = seaborn.heatmap(result_df100, annot=True, fmt=".3f", square=True)
plt.xlabel("min_cf")
plt.ylabel("min_df")
plt.gcf().set_size_inches(12, 10)

# %%
result_df500 = pd.DataFrame({i_cf*10: { i_df*10: sum(c_df)/len(c_df) for i_df, c_df in enumerate(c_cf)} for i_cf, c_cf in enumerate(c[2]) }).head(11)

# %%
fig = plt.figure()
fig.tight_layout()
p = seaborn.heatmap(result_df500, annot=True, fmt=".3f", square=True)
plt.xlabel("min_cf")
plt.ylabel("min_df")
plt.gcf().set_size_inches(12, 10)

# %%
np.average(np.array(c[6][10]))

# %%
with open('data/coherence_5030500.txt', 'rb') as p:
    coherence = pickle.load(p)

# %%
coherence = []
k_max = 41
for k in tqdm(range(5, k_max)):
    model = tomotopy.LDAModel(k=k, min_df=50, corpus=corpus, seed=1234)
    model.train(iter=500)
    coherence.append(tomotopy.coherence.Coherence(model).get_score())

# %%
fig = plt.figure()
ax1 = fig.add_subplot(111)
c1 = 'lightcoral'
ax1.set_xlabel('k')
ax1.plot(list(range(5, 41)), coherence, 'o-', color=c1)
ax1.set_ylabel('Coherence', color=c1); ax1.tick_params('y', colors=c1)
ax1.set_xticks(np.arange(5, 41, step=5))
fig.tight_layout()
plt.grid(axis="y")
fig.set_size_inches(12, 6)
plt.savefig("data/c.svg")
plt.show()

# %%
model = tomotopy.LDAModel(k=22, min_df=50, corpus=corpus, seed=1234)
model.train(iter=500)

# %%
bi_min = 0
bi_max = 10001
c_bi = []
for bi in tqdm(range(bi_min, bi_max, 100)):
    c_tmp = []
    for i in range(50):
        model = tomotopy.LDAModel(k=27, min_df=50, corpus=corpus)
        model.burn_in = bi
        model.train(iter=500)
        c_tmp.append(tomotopy.coherence.Coherence(model).get_score())
    c_bi.append(sum(c_tmp)/len(c_tmp))

# %%
fig = plt.figure()
ax1 = fig.add_subplot(111)
c1 = 'lightcoral'
ax1.set_xlabel('burn-in time')
ax1.plot(list(range(bi_min, bi_max, 100)), c_bi, 'o-', color=c1)
ax1.set_ylabel('Coherence', color=c1); ax1.tick_params('y', colors=c1)
ax1.set_xticks(np.arange(bi_min, bi_max, step=1000))
fig.tight_layout()
plt.grid(axis="y")
fig.set_size_inches(12, 6)
plt.savefig("data/c.svg")
plt.show()

# %%
with open("data/burn_in_c.txt", mode="wb") as f:
    pickle.dump(c_bi, f)

# %%
pd.DataFrame([[w[0] for w in model.get_topic_words(k, top_n=20)] for k in range(model.k)]).transpose()

# %%
pd.DataFrame([[w[0] for w in model.get_topic_words(k, top_n=20)] for k in range(model.k)]).transpose()

# %%
tomotopy.coherence.Coherence(model).get_score()

# %%
pd.DataFrame([tomotopy.coherence.Coherence(model).get_score(topic_id=k) for k in range(model.k)]).transpose()

# %%
model = tomotopy.LDAModel.load("data/model_final2")

# %%
model.burn_in = 10000

# %% [markdown]
# ## 類似度計算

# %%
vecs = {}
for r in all_reviews_rm_NE:
    vec = model.infer(model.make_doc(r["reviews"]))[0]
    vecs[r["series"]] = vec

# %%
pd.DataFrame(vecs).transpose().sort_values(14, ascending=False).head(20)

# %%
pd.DataFrame(vecs).transpose().sort_values(3, ascending=False).describe()

# %%
with open('data/vecs1.json', 'w') as f:
    json.dump({v[0] : v[1].tolist() for v in vecs.items()}, f, ensure_ascii = False)

# %% [markdown]
# ## メタデータエクスポート

# %%
db = sqlite3.connect("data/bookMeter.db")
cur = db.cursor()
#series = [s[0] for s in cur.execute("SELECT DISTINCT series FROM books").fetchall()]
series = [r["series"] for r in result]
meta_data = {}
for s in series:
    isbns = [i[0] for i in cur.execute("SELECT isbn FROM books WHERE series=?", (s, )).fetchall()]
    vols = cur.execute("SELECT volume FROM books WHERE series=?", (s, )).fetchall()
    pds = cur.execute("SELECT published_date FROM books WHERE series=?", (s, )).fetchall()
    pds = [pd[0] for pd in pds if not pd[0] is None]
    if [v[0] for v in vols].count("1") == 1:
        first_isbn = cur.execute("SELECT isbn FROM books WHERE series=? and volume=1", (s, )).fetchone()[0]
    elif len(pds) != 0:
        pds = sorted(pds, key=lambda x: int(x.replace(".", "")) if len(x) == 7 else int(x.replace(".", "") + "999"))
        first_isbn = cur.execute("SELECT isbn FROM books WHERE series=? and published_date=?", (s, pds[0])).fetchone()[0]
    else:
        first_isbn_no_cd = str(min([i[:-1] for i in isbns]))
        first_isbn = [i for i in isbns if i[:-1] == first_isbn_no_cd][0]
    page_ave = round(cur.execute("SELECT AVG(page_count) FROM books WHERE series=?", (s, )).fetchone()[0])
    other = cur.execute("SELECT author, published_date, publisher FROM books WHERE isbn=?", (first_isbn, )).fetchone()
    author = re.sub("[0-9]{4}-", "", re.sub(", ","" ,other[0]))
    meta_data[s] = {"author" : author, "first_isbn" : first_isbn, "first_pub_year" : other[1], "publisher" : other[2], "page_ave" : page_ave, "vol_num" : len(isbns)}
with open('data/meta_data.json', 'w') as f:
    json.dump(meta_data, f, indent=2 ,ensure_ascii = False)

# %% [markdown]
# ## 第一巻の書影を抽出

# %%
with open('data/meta_data.json') as f:
    meta_data = json.load(f)

# %%
for md in meta_data.values():
    f_i = md["first_isbn"]
    if os.path.isfile(f"data/shoei/{f_i}.png"):
        shutil.copy(f"data/shoei/{f_i}.png", f"data/first_shoei/")
    else:
        print(f"{f_i} skip")
        print(md["author"])


# %% [markdown]
# ## 出版年月を更新

# %%
def getPD(isbn):
    f = urlopen(f"https://iss.ndl.go.jp/api/opensearch?isbn={isbn}")
    xml = f.read()
    root = etree.fromstring(xml)
    for item in root.iter("item"):
        pd = item.find("dc:date", root.nsmap)
        if not pd is None:
            pd = unicodedata.normalize("NFKC", pd.text)
            pd = re.sub("月|(\(.+\))|-", "", re.sub("年", ".", pd))
            pd = pd[0:4] + "." + pd.split(".")[1].zfill(2) if len(pd) > 5 else pd
            pd = pd.replace(" ", "")
            break
    if pd is None:
        return None
    return pd


# %%
db = sqlite3.connect("data/bookMeter.db")
cur = db.cursor()
isbns = [i[0] for i in cur.execute("SELECT isbn from books").fetchall()]
for i in isbns:
    result = getPD(i)
    cur.execute("UPDATE books SET published_date=? WHERE isbn=?", (result, i))
    db.commit()

# %% [markdown]
# ## レビュー文抜粋機能 

# %%
model = tomotopy.LDAModel.load("data/model_final2")

# %%
distilled_student_sentiment_classifier = pipeline(
    model="lxyuan/distilbert-base-multilingual-cased-sentiments-student", 
    return_all_scores=True
)

# %%
pd.DataFrame([[w[0] for w in model.get_topic_words(k, top_n=20)] for k in range(model.k)]).transpose()

# %%
with open('data/vecs1.json') as f:
    vecs = json.load(f)
vecs_df = pd.DataFrame(vecs)

# %%
cws = [[w[0] for w in model.get_topic_words(k, top_n=10)] for k in range(model.k)]

# %%
top_topics = {}
for title, vec in vecs_df.items():
    top_topics[title] = []
    for topic_id, _ in vec.sort_values(ascending=False)[:4].items():
        top_topics[title].append(topic_id)

# %%
top_topics

# %%
cws = [["帝国", "騎士", "政治", "令嬢"], ["仲間", "成長", "活躍", "過去"], ["ヒロイン", "可愛い", "キャラ", "デート"], ["切ない", "幸せ", "美しい", "綺麗"], ["アイドル", "音楽", "部活"], ["クラス", "学園", "学校", "生徒"], 
      ["結末", "悲しい", "救い", "絶望"], ["ラブコメ", "恋愛", "二人", "甘い"], ["魔王", "ファンタジー", "勇者"], ["可愛い", "キャラ", "過去", "成長"], ["百合", "お互い", "関係性", "気持ち"], ["戦争", "戦闘", "無双", "戦い"],
     ["キャラ", "登場人物", "活躍", "かっこいい"], ["最強", "魔術", "天才", "科学"], ["日常", "高校", "青春"], ["魔法", "冒険", "パーティー"], ["ネタ", "ギャグ", "コメディ"], ["可愛い", "イラスト"], ["人生", "幸せ", "幸福", "美しい"],
     ["展開", "伏線", "思惑", "世界"], ["描写", "文章", "表現", "内容"], ["バトル", "熱い", "戦い"], ["転生", "異世界", "前世"], ["ゲーム", "勝負", "ライバル", "才能"], ["世界観", "SF", "設定", "能力"], ["未来", "思春期", "青春"], ["事件", "ミステリ", "推理", "トリック"]]


# %%
def getReviews(title, cw):
    distilled_student_sentiment_classifier = pipeline(
        model="lxyuan/distilbert-base-multilingual-cased-sentiments-student", 
        return_all_scores=True
    )
    db = sqlite3.connect("data/bookMeter.db")
    cur = db.cursor()
    sql = "SELECT review, star, series FROM reviews INNER JOIN books ON reviews.isbn = books.isbn WHERE series=? AND ( "
    for i, _ in enumerate(cw):
        sql += "review LIKE ? "
        if (i + 1) != len(cw):
            sql += 'OR '
    sql += ') ORDER BY star DESC LIMIT 500;'
    result = cur.execute(sql, [title] + list(map(lambda w: "%" + w + "%", cw))).fetchall()
    db.close()
    result = [r[0] for r in result if judgeP(r[0])][:100]
    result = sum([re.split("[。．]", r) for r in result], [])
    result = [r for r in result if len(r) < 100 and len(r) > 15]
    return sorted(result, key=lambda s: sum([s.count(w) for w in cw]), reverse=True)[:4]


# %%
model.infer(model.make_doc("３つの不可解な殺人事件のお供をするのは、今日子さんの推理によって事件を解決しようとする肘折警部、遠浅警部、鈍麻警部..."))[0][1]


# %%
def getReviews2(title, k):
    lab_dic_p = "/usr/lib/x86_64-linux-gnu/mecab/dic/mecab-ipadic-neologd"
    db = sqlite3.connect("data/bookMeter.db")
    cur = db.cursor()
    sql = "SELECT review, star, series FROM reviews INNER JOIN books ON reviews.isbn = books.isbn WHERE series=? ORDER BY star DESC LIMIT 500;"
    result = cur.execute(sql, (title, )).fetchall()
    db.close()
    result = [r[0] for r in result]
    result = sum([re.split("[。．]", r) for r in result], [])
    result = [r for r in result if len(wakachiGaki(sentence=r, dic_path=lab_dic_p, resque=["百合"])) != 0]
    result = sorted(result, key = lambda x:model.infer(model.make_doc(wakachiGaki(sentence=x, dic_path=lab_dic_p, resque=["百合"])))[0][k], reverse=True)[:100]
    
    return [r for r in result if len(r) < 100 and len(r) > 20][:4]


# %%
def judgeP(review):
    distilled_student_sentiment_classifier = pipeline(
        model="lxyuan/distilbert-base-multilingual-cased-sentiments-student", 
        return_all_scores=True
    )
    return distilled_student_sentiment_classifier(review)[0][0]['score'] > 0.6


# %%
getReviews2("人類は衰退しました", 17)

# %%
reviews_excerpted = {}
for title in tqdm(top_topics.keys()):
    reviews_excerpted[title] = {t: getReviews(title, cws[t]) for t in top_topics[title]}

# %%
reviews_excerpted

# %%
with open('data/reviews_excerpted.json', 'w') as f:
    json.dump(reviews_excerpted, f, indent=2, ensure_ascii=False)

# %%
with open('data/characteristic_word.json', 'w') as f:
    json.dump(cws, f, indent=2, ensure_ascii=False)

# %% [markdown]
# ## ワードクラウド

# %%
model = tomotopy.LDAModel.load("data/model_final2")

# %%
model.get_topic_words(0)

# %%
word_cloud = WordCloud(font_path='ipag.ttf', width=1600, height=900,
                        min_font_size=5, 
                        collocations=False, background_color='white', colormap='tab20',
                        max_words=400).generate_from_frequencies({v[0]: v[1] for v in model.get_topic_words(10,top_n=50)})

# %%
plt.figure(figsize=(16, 9))
plt.imshow(word_cloud, interpolation="bilinear")
plt.axis("off")
plt.savefig('data/wordclouds/10.svg')

# %%
for k in range(model.k):
    word_cloud = WordCloud(font_path='ipag.ttf', width=1600, height=900,
                        min_font_size=5, 
                        collocations=False, background_color='white', colormap='tab20',
                        max_words=400).generate_from_frequencies({v[0]: v[1] for v in model.get_topic_words(k, top_n=50)})
    plt.figure(figsize=(16, 9))
    plt.imshow(word_cloud, interpolation="bilinear")
    plt.axis("off")
    plt.savefig(f"data/wordclouds/{k}.svg")

# %%
