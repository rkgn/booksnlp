# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.7
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %%
import json, sqlite3, tomotopy, requests, seaborn
from tqdm.auto import tqdm
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np

# %%
with open('data/reviews4.json') as f:
    all_reviews = json.load(f)
corpus = tomotopy.utils.Corpus.load("data/corpus4")

# %%
coherence = []
perplexity = []
for k2 in tqdm(range(10, 61)):
    model = tomotopy.PAModel(k1=10, k2=k2, min_cf=25, min_df=30, corpus=corpus, rm_top=5)
    model.train(iter=100)
    coherence.append(tomotopy.coherence.Coherence(model).get_score())
    perplexity.append(model.perplexity)

# %%
db = sqlite3.connect("data/bookMeter.db")
cur = db.cursor()
result = []
for s in tqdm([r[0] for r in cur.execute("select distinct series from books").fetchall()]):
    isbns = [i[0] for i in cur.execute("select isbn from books where series = ?", (s, ))]
    cnt = [cur.execute("select sum(length(review)) from reviews where isbn = ?", (isbn, )).fetchone()[0] for isbn in isbns]
    cnt = sum(filter(None, cnt))
    if cnt >= 10000:
        result.append({"series" : s, "word_cnt" : cnt })
result

# %%
sw = ["シリーズ", "ロレンス", "スバル", "しまむら", "マイン", "上条さん", "物語シリーズ", "ベル", "作品", "アリシゼーション", "キノ", "物語", "作品", "ない", "いい", "好き", "再読", "イクタ", "阿良", "八幡", "撫子", "羽川",
      "カリエ", "インデックス", "ミーア", "ゴブリンスレイヤー", "ナザリック", "アインズ", "アリソン", "リムル", "クトリ", "ヴィレム","ラノベ", "ライトノベル", "アニメ", "主人公", "あとがき", "大河", "藻屑", "コル", "面白かっ",
     "一方通行", "小説", "キノの旅" ,"学園都市", "雪ノ下", "セルティ", "帝人", "平塚", "由比ヶ浜", "友崎", "アド", " ケイ", "杏里", "戯言シリーズ", "文学少女", "ちーちゃん", "理科", "ミミズク", "シエスタ", "夜空", "いろはす",
     "銀子", "読了", "シズちゃん", "バッカーノ", "九曜", "やせ", "エンテイスラ", "アイリス", "真昼", "白粉", "まーちゃん", "フルメタ", "ケイ", "ホライゾン", "デュラララ", "バカテス", "かなめ", "本編", "ウィズ", "戯言", "彼ら",
     "バカテス", "成田", "化物語", "86", "ヒッキー", "ページ", "シーズン", "シーン", "ティー", "あい", "サイド", "新約", "池袋", "さま", "モモ", "たつ", "部分", "ほしい", "SOS団", "古泉", "潤さん", "クビシメロマンチスト",
     "コウ", "サクラ", "ノット", "シカ", "ザカリア", "すごい", "BACCANO!", "アイザック", "良い", "多い", "よい", "カラ", "東和", "完結", "テン", "トエ", "からすみ", "クロハ", "フェリオ", "パンプキン", "リセ", "7 Iris",
     "ALISON", "メグセロ", "リリトレ", "リリアとトレイズ", "ドクロちゃん", "ジーク", "長編", "砂糖", "短編", "長い", "代行", "Hickey", "DELE", "無い", "短編集", "秀麗", "症候群", "お話", "ライダー", "Fate", "Web", "妖精さん",
     "やすい", "古城", "古典部", "前半", "後半", "少佐", "時雨", "弁当", "一番", "原作", "娘。", "後宮", "グレイ", "マスター", "既読", "アニメ化", "禁書", "面白い", "2人", "ミステリ", "最後", "師匠", "SS", "印象", "エピローグ",
     "作家", "最終", "嬉しい", "登場", "リセット", "今後", "期待", "強い", "弟子", "楽しみ", "意味", "日本", "期待", "外伝", "パーティ", "タイトル", "最初", "欲しい", "大きい", "続く", "っぽい", "問題", "楽しい",
     "作り", "っぷり", "厄介", "プロローグ", "非常", "サイト", "偽物", "ただ", "ラスト", "デビュー作", "にくい", "おもしろい", "周り", "軽い", "メイン", "状況", "説明", "Kindle", "エロ", "おっぱい", "出し手", "すぎ", "劇場版",
     "づらい", "個人的", "続編", "書籍"]
db = sqlite3.connect("data/bookMeter.db")
cur = db.cursor()
series = [r["series"] for r in result]
sw = sw + series
db.close()
url = "http://svn.sourceforge.jp/svnroot/slothlib/CSharp/Version1/SlothLib/NLP/Filter/StopWord/word/Japanese.txt"
sw = [sw for sw in requests.get(url).text.split() if sw != ""] + sw

# %%
with open('data/reviews_rm_NE_max_5e4.json') as f:
    all_reviews_rm_NE_max_5e4 = json.load(f)

# %%
corpus = tomotopy.utils.Corpus(stopwords=sw)
[corpus.add_doc(r["reviews"]) for r in tqdm(all_reviews_rm_NE_max_5e4)];

# %%
coherence = np.zeros((36, 36))
index = columns = [i for i in np.arange(5, 41)]
c_df = pd.DataFrame(coherence, index=index, columns=columns)

# %%
# %%time
k1_max = 41
k2_max = 41
for k1 in tqdm(range(5, k1_max)):
    for k2 in range(k1, k2_max):
        model = tomotopy.PAModel(k1=k1, k2=k2, min_df=50, corpus=corpus)
        model.train(iter=500)
        c_df[k1][k2] = tomotopy.coherence.Coherence(model).get_score()

# %%
model = tomotopy.PAModel(k1=10, k2=13, min_df=50, corpus=corpus)
model.train(iter=500)

# %%
for k in range(model.k):
    sub_ks = [sub_k[0] for sub_k in model.get_sub_topics(k)][:6]
    print(f"Super Topic {k+1}")
    for sub_k in sub_ks:
        words = [w[0] for w in model.get_topic_words(sub_topic_id=sub_k, top_n=10)]
        print(words)

# %%
fig = plt.figure()
fig.tight_layout()
p = seaborn.heatmap(c_df, annot=True, fmt=".3f", square=True)
plt.xlabel("k1")
plt.ylabel("k2")
plt.gcf().set_size_inches(30, 20)

# %%
c_df.to_pickle('data/pam_c_df.pkl')

# %%
