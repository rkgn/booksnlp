# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.7
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %%
import json, requests, urllib.parse, re, time, Levenshtein
from tqdm.auto import tqdm

# %%
with open('data/meta_data.json') as f:
    md = json.load(f)

# %%
md


# %%
def getSummary(title, author):
    time.sleep(1)
    URL = "https://ja.wikipedia.org/w/api.php"
    SEARCH_PARAMS = {
        "action": "query",
        "format": "json",
        "list": "search",
        "srsearch": f"{title} 小説 {author}"
    }
    res = requests.get(url=URL, params=SEARCH_PARAMS)
    top5 = res.json()['query']['search']
    top5 = sorted(top5, key = lambda x: Levenshtein.ratio(title, x['title']), reverse=True)[:5]
    title_wiki = top5[0]['title']
    print(title)
    print(Levenshtein.ratio(title, title_wiki))
    if Levenshtein.ratio(title, title_wiki) < 0.35:
        return ''
    pageid = top5[0]['pageid']

    ARTICLE_PARAMS = {
        "action": "query",
        "format": "json",
        "prop": "extracts",
        "explaintext": True,
        "exsectionformat": "plain",
        "pageids": pageid
    }
    time.sleep(1)
    res = requests.get(url = URL, params = ARTICLE_PARAMS)
    if res.status_code != 200:
        return ''
    try:
        text = res.json()['query']['pages'][str(pageid)]['extract']
    except:
        return ''
    articles = text.split("\n\n\n")
    for i, article in enumerate(articles):
        if "あらすじ" in article and len(article) > 10:
            return articles[0] + re.sub("あらすじ\n", "", article)
        article = "\n".join(articles[:(i+1)])
        if len(article) > 500:
            return article


# %%
getSummary('ステラ・ステップ', '林星悟')

# %%
for title in tqdm(md):
    md[title]['summary'] = getSummary(title, md[title]['author'])

# %%
md

# %%
with open('data/meta_data.json', 'w') as f:
    json.dump(md, f, indent=2 ,ensure_ascii = False)

# %%
with open('data/meta_data.json') as f:
    md = json.load(f)

# %%
for s in md.keys():
    if len(md[s]['summary']) == 0:
        print(s)

# %%
md['ステラ・ステップ']

# %%
md

# %%
