# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.7
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %%
import json, hashlib
import pandas as pd
import matplotlib.pyplot as plt

# %%
with open("data/surveys.json", "r") as f:
    surveys = json.load(f)

# %%
surveys = [{answer[0]: answer[1] for answer in sorted(survey.items())} for survey in surveys]

# %%
for i, survey in enumerate(surveys):
    surveys[i]["name"] = hashlib.md5(survey["name"].encode()).hexdigest()

# %%
result_df = pd.DataFrame(surveys)

# %%
result_df

# %%
result_df.describe()

# %%
result_df.select_dtypes(exclude="number")

# %%
result_df["q4.3"]

# %%
[print(s + "\n") for s in result_df["q5.4"]];

# %%
surveys_other = [{"name": s["name"], "title_mode_query": surveys[i].pop('title_mode_query'), "title_mode_survey": surveys[i].pop('title_mode_survey'), "topic_mode_query": surveys[i].pop('topic_mode_query'), "topic_mode_survey": surveys[i].pop('topic_mode_survey')} for i, s in enumerate(surveys)]

# %%
title_mode_survey = [s['title_mode_survey'] for s in surveys_other]


# %%
def calcILAvg(surveys):
    return sum([s['interest_level'] for s in surveys]) / len(surveys)


# %%
result_title = [calcILAvg(s) for s in title_mode_survey]

# %%
pd.DataFrame(result_title).describe()

# %%
topic_mode_survey = [s['topic_mode_survey'] for s in surveys_other]

# %%
result_topic = [calcILAvg(s) for s in topic_mode_survey]

# %%
pd.DataFrame(result_topic).describe()

# %%
pd.crosstab(result_df['q1.4'], result_df['q4.1'])

# %%
title_mode_survey

# %%
sim_list_title = sorted(sum(title_mode_survey, []), key=lambda s: s["similarity"])

# %%
x = []
y = []
for s in sim_list_title:
    x.append(s['similarity'])
    y.append(s['interest_level'])

# %%
fig = plt.figure()
ax1 = fig.add_subplot(111)
c1 = 'lightcoral'
ax1.set_xlabel('similarity')
ax1.plot(x, y, 'o', color=c1)
ax1.set_ylabel('interest level', color=c1); ax1.tick_params('y', colors=c1)
#ax1.set_xticks(np.arange(10, 1001, step=10))
fig.tight_layout()
plt.grid(axis="y")
fig.set_size_inches(5, 5)
plt.show()

# %%
x_s = pd.Series(x)
y_s = pd.Series(y)
x_s.corr(y_s)


# %%
def cntInterestBook(surveys):
    interest = [s['is_alr_read'] for s in sum(surveys, []) if s['interest_level'] > 2]
    interest_cnt = len(interest)
    interest_not_read_cnt = interest.count(False)
    return [interest_cnt, interest_not_read_cnt]


# %%
cntInterestBook(topic_mode_survey)

# %%
